# shiny-base

Base image for OSUG Shiny apps

## Usage example

### Dockerfile

```Dockerfile
FROM gricad-registry.univ-grenoble-alpes.fr/osug/dc/shiny-base:4.0.2

# Install dependencies and clean tmp files
RUN install.r \
      fmsb \
      knitr \ 
      && rm -rf /tmp/downloaded_packages

# Copy app files to /srv/shiny
COPY server.R ui.R /srv/shiny/
```

Default base image will run `R -e shiny::runApp('/srv/shiny')`, you can override this behavior by adding a `CMD` statement:

```Dockerfile
CMD ["R", "-e", "shiny::run_01_hello()"]
```

### Gitlab CI

First you need to activate Docker registry in your Project : `Settings → General → Visibility, project features, permissions → Container registry`

Here is a `.gitlab-ci.yml` that should work out-of-the box :

```yaml
image: gricad-registry.univ-grenoble-alpes.fr/kubernetes-alpes/buildah:latest

stages:
  - build

variables:
  REGISTRY_LOGIN: buildah login -u gitlab-ci-token -p $CI_REGISTRY_PASSWORD
  REGISTRY_LOGOUT: buildah logout
  IMAGE_BUILD: buildah build-using-dockerfile --storage-driver vfs --format docker
  IMAGE_PUSH: buildah push --storage-driver vfs

before_script:
- $REGISTRY_LOGIN $CI_REGISTRY

after_script:
- $REGISTRY_LOGOUT $CI_REGISTRY

docker build:
  stage: build
  variables:
    DOCKERFILE: Dockerfile
    IMAGE_NAME: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
  script:
    - $IMAGE_BUILD --file $DOCKERFILE $BUILD_ARG --tag $IMAGE_NAME .
    - $IMAGE_PUSH $IMAGE_NAME $IMAGE_NAME
```

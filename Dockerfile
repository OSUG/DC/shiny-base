ARG RVERSION
FROM r-base:$RVERSION

RUN useradd -s /bin/bash shiny

RUN chown -R shiny:shiny /usr/local/lib/R/site-library
RUN install.r shiny && rm -rf /tmp/downloaded_packages

RUN mkdir /srv/shiny && chown shiny:shiny /srv/shiny
COPY .Rprofile /srv/shiny

RUN apt-get update && apt-get install --no-install-recommends -y \
      libcurl4-openssl-dev \
      libssl-dev \
      && rm -rf /var/lib/apt/lists/*

USER shiny

WORKDIR /srv/shiny
CMD ["R", "-e", "shiny::runApp('/srv/shiny')"]
